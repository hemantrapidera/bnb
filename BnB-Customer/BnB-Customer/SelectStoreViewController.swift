//
//  SelectStoreViewController.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/1/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit
import AddressBook
import MobileCoreServices
import MapKit
class SelectStoreViewController: UIViewController,APICallsDelegate {
    var addressBook: ABAddressBookRef?
    @IBOutlet var mapView: GMSMapView!
    var getCategory = NSMutableArray ()
    let locationManager = CLLocationManager()
    let apiCallsObj = APICalls.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        print(getCategory)
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        // Do any additional setup after loading the view.
        
        let camera = GMSCameraPosition.cameraWithLatitude(18.474399,
            longitude: 73.836365, zoom: 100)
      // self.mapView = GMSMapView.mapWithFrame(self.mapView.frame, camera: camera)
        self.mapView.camera = camera
        mapView.myLocationEnabled = true
        //self.view = self.mapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(18.574562, 73.774363)
        marker.title = "My Home"
        marker.snippet = "Pune"
        marker.map = mapView
        
        let location = CLLocation(latitude: 18.574562 as CLLocationDegrees, longitude: 73.774363 as CLLocationDegrees)

         addRadiusCircle(location)
        
      //  ContactsImporter.importContacts(showContacts)
        // singltonObj.loginUser("sagar", withPassword: "123")
       // apiCallsObj.delegate = self
      //  apiCallsObj.logOutUser()
        let dict:NSArray = NSArray()
       // apiCallsObj.getAllGenresListFromDatabase()
        
    }
    
    func showContacts(contacts: Array<Contact>, error: NSError!) {
        print(contacts)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- APICallsDelegate Method
    func didSucceedRequest(request:NSURLRequest, withResponse response:NSHTTPURLResponse, andJSONData JSON:AnyObject)
    {
        let dictionary:NSDictionary = JSON as! NSDictionary
        print(dictionary)
    }
    func didFailedRequest(request:NSURLRequest, withResponse response:NSHTTPURLResponse, withError error:NSError, andJSONData JSON:AnyObject){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func addRadiusCircle(location: CLLocation){
       // self.mapView.delegate = self
        let circleCenter = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);

        
        let circ2 = GMSCircle(position: circleCenter, radius: 1000)
        circ2.fillColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.3)
        circ2.strokeColor = UIColor.darkGrayColor()
        //circ.strokeWidth = 5
        circ2.map = mapView;

        let circ1 = GMSCircle(position: circleCenter, radius: 500)
        circ1.fillColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.3)
        circ1.strokeColor = UIColor.darkGrayColor()
        //circ.strokeWidth = 5
        circ1.map = mapView;
        
        let circ = GMSCircle(position: circleCenter, radius: 100)
        circ.fillColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.3)
        circ.strokeColor = UIColor.darkGrayColor()
        //circ.strokeWidth = 5
        circ.map = mapView;
        
        
       
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.redColor()
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return nil
        }
    }
    

}

//MARK: - CLLocationManagerDelegate Methods 
extension SelectStoreViewController: CLLocationManagerDelegate {
    // 2
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 3
        if status == .AuthorizedWhenInUse {
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            mapView.myLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    // 6
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            // 7
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            // 8
            locationManager.stopUpdatingLocation()
        }
        
    }
}