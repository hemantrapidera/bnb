//
//  CompleteProfileViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 1/29/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class CompleteProfileViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate {
    @IBOutlet weak var nameTxtFld: UITextField!
    
    @IBOutlet weak var stateTxtFld: UITextField!
    @IBOutlet weak var cityTxtFld: UITextField!
    @IBOutlet weak var pinCodeTxtFld: UITextField!
    @IBOutlet weak var setPasswordTxtFld: UITextField!
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    let limitLength = 10
    var IsTnCChecked = Bool ()
    
    override func viewDidLoad() {
    }
    
    // MARK: - ButtonMethod
    @IBAction func onClickTNCBtn(sender: UIButton) {
        
        if sender.selected == true{
            IsTnCChecked = false
            
            sender.selected = false
        }else{
            IsTnCChecked = true
            sender.selected = true
        }
    }
    
    @IBAction func OnClickSaveButton(sender: UIButton) {
        if self.nameTxtFld.text == "" {
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Enter your fullname"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        }else if self.stateTxtFld .text == "" {
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Please select state"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        }else if self.cityTxtFld.text == ""{
            
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Please select city"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        } else if pinCodeTxtFld.text == ""{
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Enter pincode number"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        }/*else if setPasswordTxtFld.text == ""{
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Enter password number"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        }else if confirmPasswordTxtFld.text == "" {
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Please confirm password"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        
        }else if setPasswordTxtFld.text != confirmPasswordTxtFld.text {
            
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Set password & confirm password should be same"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
            
            }
        */else if IsTnCChecked == false {
            
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Please accept terms & conditions"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
            
        } else {
            
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Thank you" + " "+self.nameTxtFld.text! + " " + "for registering"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = self
            alertb.tag = 1000
            alertb.show()
        }
    }
    
    //MARK:- Alertview Delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        if alertView.tag == 1000 {
            if buttonIndex == 0 {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    // MARK: - TextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        self.view.endEditing(true)
        return false
    }
    
   /*  func textFieldDidBeginEditing(textField: UITextField) {
       
        if textField.tag == 4 {
            animateViewMoving(true, moveValue: 60)
        }
        
        if textField.tag == 5{
            animateViewMoving(true, moveValue: 80)
            
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag == 4 {
            animateViewMoving(false, moveValue: 60)
        }
        
        if textField.tag == 5 {
            animateViewMoving(false, moveValue: 80)
            
        }
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:NSTimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }*/
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print(string)
        print(range)
        if textField.tag == 0
        {
            
            
            
            let aSet = NSCharacterSet(charactersInString:"abcdefghijklmnopqrstuvwxyzABCDEGHIJKLMOPQRSTUVWXYZ").invertedSet
            let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
            let numberFiltered = compSepByCharInSet.joinWithSeparator("")
            return string == numberFiltered
            /*    let text = string
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength*/
            
            
        }
        if textField.tag == 1{
            
        }
        if textField.tag == 2 {
            
        }
        if textField.tag == 3 {
            
            let aSet = NSCharacterSet(charactersInString:"0123456789").invertedSet
            let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
            let numberFiltered = compSepByCharInSet.joinWithSeparator("")
            
            // return string == numberFiltered
            var tempStr = Bool ()
            tempStr = string == numberFiltered
            
            if textField.text?.characters.count > 8 {
                return false;
            }else {
                return tempStr;
                
            }
        }
       
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
