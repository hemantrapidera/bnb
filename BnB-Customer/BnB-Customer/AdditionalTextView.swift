//
//  AdditionalTextView.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/2/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit
protocol AdditionalTextViewDelegate {
    func additionalTextViewClicked(txtView: AdditionalTextView, sender:UIButton)
}

class AdditionalTextView: UIView,UITextFieldDelegate {

    var delegate:AdditionalTextViewDelegate?
    
    override func drawRect(rect: CGRect) {
        
    }

    var s: String?
    var i: Int?
    init(i: Int) {
        super.init(frame: CGRect(x: 15, y: 15, width: 150, height: 40))
        
        let button   = UIButton(type: UIButtonType.Custom) as UIButton
        button.frame = CGRectMake(0,0,150,40)
        button.backgroundColor = UIColor.yellowColor()
        button.tag = i
        //button.setTitle("Test Button", forState: UIControlState.Normal)
        button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(button)
        
        
        let txtField = UITextField(frame: CGRectMake(00, 05, 100, 30))
        txtField.placeholder = "Add text"
        txtField.font = UIFont.systemFontOfSize(10)
        txtField.borderStyle = UITextBorderStyle.None
        txtField.autocorrectionType = UITextAutocorrectionType.No
        txtField.keyboardType = UIKeyboardType.Default
        txtField.returnKeyType = UIReturnKeyType.Done
        txtField.backgroundColor = UIColor.orangeColor()
        //txtField.clearButtonMode = UITextFieldViewMode.WhileEditing;
        //txtField.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        txtField.delegate = self
        self.addSubview(txtField)
        
        
    }

    func buttonAction(sender:UIButton){
        print("button clicked with tag: \(sender.tag)")
        delegate?.additionalTextViewClicked(self, sender: sender)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
