//
//  APICalls.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/4/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit
protocol APICallsDelegate {
    //success
    func didSucceedRequest(request:NSURLRequest, withResponse response:NSHTTPURLResponse, andJSONData JSON:AnyObject)
    //failure
    func didFailedRequest(request:NSURLRequest, withResponse response:NSHTTPURLResponse, withError error:NSError, andJSONData JSON:AnyObject)
}
class APICalls: NSObject {
    var delegate:APICallsDelegate?
    var singleton:APICalls!
    var baseURL = "http://ec2-54-169-245-217.ap-southeast-1.compute.amazonaws.com/mobile_api"
    class var sharedInstance: APICalls {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: APICalls? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = APICalls()
        }
        return Static.instance!
    }
    
    
    override init() {
        super.init()
    }
    
    class func sharedOperationQueue()->NSOperationQueue{
        var sharedOperationQueue:NSOperationQueue! = nil
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken) {
            sharedOperationQueue = NSOperationQueue()
            sharedOperationQueue.maxConcurrentOperationCount = 1
        }
        return sharedOperationQueue
        
    }
    
    func sharedAppDelegate()->AppDelegate{
        return  UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    //MARK:- POST METHODs
    func loginUser(userID:NSString,withPassword password:NSString){
        let dict:NSMutableDictionary = NSMutableDictionary()
        dict.setObject("1234567890", forKey: "MobileNo")
        dict.setObject("xyz", forKey: "CustomerName")
        dict.setObject("1", forKey: "ChannelType")
        dict.setObject("asd", forKey: "Comments")
        dict.setObject("saassaaa", forKey: "SecurityToken")
        
        let url:NSURL = NSURL(string: "http://vcorporation.co.in/bnbfintech/wallet/GenerateCard")!
        do {
            let jsonData:NSData = try NSJSONSerialization.dataWithJSONObject(dict, options: .PrettyPrinted)
            
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for Login function \(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            // request.setValue("application/json", forKey: "X-API-KEY")
            let str:NSString = NSString(format: "%lu", jsonData.length)
            request.setValue(str as String, forHTTPHeaderField: "Content-Length")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "")
                    {
                        if (self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON) != nil){
                            self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                        }
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "")
                    {
                        if (self.delegate?.didFailedRequest(request, withResponse: response, withError:error, andJSONData: JSON) != nil){
                            self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                        }
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - Generate Order
    func generateOrder(orderDetail:NSMutableDictionary){
        let orderDetail = NSMutableDictionary()
        orderDetail.setObject("1", forKey: "merchant_id")
        orderDetail.setObject("1", forKey: "customer_id")
        orderDetail.setObject("200", forKey: "order_amt")
        let arr = NSArray(objects: NSDictionary(objects: NSArray(objects: "1","1","2","60","30","") as [AnyObject], forKeys: NSArray(objects: "product_id","genre_id","qty","amount","price","image_url") as! [NSCopying]),NSDictionary(objects: NSArray(objects: "2","1","1","15","15","") as [AnyObject], forKeys: NSArray(objects: "product_id","genre_id","qty","amount","price","image_url") as! [NSCopying]))
        orderDetail.setObject(arr, forKey: "product_list")
        
        let strurl = NSString(format: "%@/order/save_order", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(orderDetail, options: NSJSONWritingOptions.PrettyPrinted)
            
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for save_order \(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "save_order")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "save_order")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - Remove Item From Order
    func removeItems(itemIds:NSArray, ForOrder orderId:NSString){
        let item = NSMutableDictionary()
        item.setObject("1", forKey: "order_id")
        item.setObject("2", forKey: "product_id")
        let strurl = NSString(format: "%@/order/remove_item", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(item, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for remove item for order number\(orderId)\n \(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "remove_item")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "remove_item")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - update order From Order
    func updateOrder(orderDetail:NSArray, removedProductIds arrRemovedProducts:NSArray, ForOrder orderId:NSString){
        let updatedOrder = NSMutableDictionary()
        updatedOrder.setObject("1", forKey: "order_id")
        let arr1:NSArray = NSArray(objects: "1","2","3") as [AnyObject]
        updatedOrder.setObject(arr1, forKey: "removed_product_ids")
        let arr = NSArray(objects: NSDictionary(objects: NSArray(objects: "1","2","60","30") as [AnyObject], forKeys: NSArray(objects: "product_id","qty","amount","price") as! [NSCopying]),NSDictionary(objects: NSArray(objects: "2","1","15","15") as [AnyObject], forKeys: NSArray(objects: "product_id","qty","amount","price") as! [NSCopying]))
        updatedOrder.setObject(arr, forKey: "updated_product_list")
        //updatedOrder.setObject(orderDetail, forKey: "product_list") //uncomment this line

        let strurl = NSString(format: "%@/order/update_order", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(updatedOrder, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for update Order for order number\(orderId)\n \(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "update_order")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "update_order")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - import Contacts
    func saveContactsToDatabase(contactList:NSArray){
        let contactDict = NSMutableDictionary()
        let arr = NSArray(objects: NSDictionary(objects: NSArray(objects: "John","123456789","Home","1","1") as [AnyObject], forKeys: NSArray(objects: "name","contact_no","type","merchant_id","customer_id") as! [NSCopying]),NSDictionary(objects: NSArray(objects: "John","123456789","Home","1","1") as [AnyObject], forKeys: NSArray(objects: "name","contact_no","type","merchant_id","customer_id") as! [NSCopying]))
        contactDict.setObject(arr, forKey: "contact_list")
        let strurl = NSString(format: "%@/contact/import_contacts", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(contactDict, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for Conatct list import \(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "import_contacts")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "import_contacts")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - get genres list
    func getAllGenresListFromDatabase(){
       
        let strurl = NSString(format: "%@/genre/genre_list", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
            if((JSON) != nil)
            {
                if(request.URL?.lastPathComponent == "genre_list")
                {
                    self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                }
            }
            },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                if(request.URL?.lastPathComponent == "genre_list")
                {
                    print(error)
                    self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                }
        })
        
        self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
        
    }
    
    //MARK:- Local APIS - OTP Request
    func requestForOTPToServer(language:NSString, forMobileNumber mobileNo:NSString){
        let dict = NSMutableDictionary()
        dict.setObject(language, forKey: "language")
        dict.setObject(mobileNo, forKey: "mobile_no")
        let strurl = NSString(format: "%@/customer/request_otp", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dict, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for OTP generation\(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "request_otp")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "request_otp")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - RESET OTP Request
    func resetOTPForMobileNumber(mobileNo:NSString){
        let dict = NSMutableDictionary()
        dict.setObject(mobileNo, forKey: "mobile_no")
        let strurl = NSString(format: "%@/customer/reset_otp", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dict, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for OTP generation\(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "reset_otp")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "reset_otp")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    //MARK:- Local APIS - Validate OTP Request
    func validateOTPForMobileNumber(mobileNo:NSString, withOTP otp:NSString){
        let dict = NSMutableDictionary()
        dict.setObject(mobileNo, forKey: "mobile_no")
        dict.setObject(otp, forKey: "otp")
        let strurl = NSString(format: "%@/customer/validate_otp", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dict, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for OTP validation\(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "validate_otp")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "validate_otp")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    
    //MARK:- Local APIS - Complete Profile
    func updateProfile(profileDict:NSMutableDictionary){
        let profileDict = NSMutableDictionary()
        profileDict.setObject("ABC", forKey: "firstname")
        profileDict.setObject("XYZ", forKey: "lastname")
        profileDict.setObject("MH", forKey: "state")
        profileDict.setObject("Pune", forKey: "city")
        profileDict.setObject("416012", forKey: "pincode")
        profileDict.setObject("123456", forKey: "password")
        profileDict.setObject("1", forKey: "customer_id")
        
        let strurl = NSString(format: "%@/customer/complete_profile", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(profileDict, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonString:NSString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            print("input param for profile updation\(jsonString)");
            let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
                if((JSON) != nil)
                {
                    if(request.URL?.lastPathComponent == "complete_profile")
                    {
                        self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                    }
                }
                },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                    if(request.URL?.lastPathComponent == "complete_profile")
                    {
                        print(error)
                        self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                    }
            })
            
            self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
            
        } catch {
            print(error)
        }
        
        
    }
    
    
    //MARK:- Local APIS - State List
    func getStateList(){
        let strurl = NSString(format: "%@/constants/state_list", baseURL)
        let url:NSURL = NSURL(string: strurl as String)!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
            if((JSON) != nil)
            {
                if(request.URL?.lastPathComponent == "state_list")
                {
                    self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                }
            }
            },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                if(request.URL?.lastPathComponent == "state_list")
                {
                    print(error)
                    self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                }
        })
        
        self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
        
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- GET METHODs
    func logOutUser(){
        let url:NSURL = NSURL(string: "http://vcorporation.co.in/bnbfintech/wallet/GenerateCard?MobileNo=1234567890&CustomerName=xy&ChannelType=shdfhjgshdf&Comments=dsfdsdsfsfd&SecurityToken=fsddfsffsfdfsdfsdfsfsdsfsdfdfs")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 120.0)
        request.HTTPMethod = "GET"
        // request.setValue("application/json", forKey: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        // request.setValue("application/json", forKey: "X-API-KEY")
        
        let jsonOperation:AFJSONRequestOperation = AFJSONRequestOperation(request: request, success: { (request:NSURLRequest!, response:NSHTTPURLResponse!, JSON:AnyObject!) -> Void in
            if((JSON) != nil)
            {
                if(request.URL?.lastPathComponent == "GenerateCard")
                {
                    self.delegate?.didSucceedRequest(request, withResponse: response, andJSONData: JSON)
                }
            }
            },failure: { (request:NSURLRequest!, response:NSHTTPURLResponse!,error:NSError!, JSON:AnyObject!) -> Void in
                if(request.URL?.lastPathComponent == "")
                {
                    self.delegate?.didFailedRequest(request, withResponse: response,withError:error, andJSONData: JSON)
                }
        })
        
        self.classForCoder.sharedOperationQueue().addOperation(jsonOperation)
    }
}
