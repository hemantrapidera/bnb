//
//  Singleton.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/4/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class Singleton: NSObject {
    var singleton:Singleton!
    class var sharedInstance: Singleton {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: Singleton? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = Singleton()
        }
        return Static.instance!
    }
    
    
    override init() {
        super.init()
    }
    
    class func sharedOperationQueue()->NSOperationQueue{
       var sharedOperationQueue:NSOperationQueue! = nil
        var onceToken: dispatch_once_t = 0
        dispatch_once(&onceToken) {
            sharedOperationQueue = NSOperationQueue()
            sharedOperationQueue.maxConcurrentOperationCount = 1
        }
        return sharedOperationQueue
        
    }
   
    func sharedAppDelegate()->AppDelegate{
        return  UIApplication.sharedApplication().delegate as! AppDelegate
    }
    

    
}
