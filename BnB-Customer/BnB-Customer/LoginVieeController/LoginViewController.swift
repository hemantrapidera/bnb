//
//  LoginViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 1/29/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    var countryCode = NSString ()
    var countryCodeLength : Int = 0
    
    @IBOutlet weak var countryCodeTxtFld: UITextField!
    @IBOutlet weak var mobileNumTxtFld: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem .setHidesBackButton(true, animated: true)
        let cc = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as! String
        let dataSource = CountryCodeDataSource ()
        countryCode = dataSource.countryCodeForCountry(cc)
        countryCodeLength = countryCode.length
        self.countryCodeTxtFld.text = countryCode as String
        print(countryCode)
    }

    
    //Phone number validation
    func phoneNumberValidation(value: String) -> Bool {
        let
        charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        var filtered:NSString!
        let inputString:NSArray = value.componentsSeparatedByCharactersInSet(charcter)
        filtered = inputString.componentsJoinedByString("")
        return  value == filtered
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        self.view.endEditing(true)
        return false
    }


    @IBAction func onClickSubmitButton(sender: UIButton) {
        
        if self.phoneNumberValidation(mobileNumTxtFld.text!) == false  {
            
            let alert = UIAlertView()
            alert.title = "Message"
            alert.message = "Enter Valid Contact Number"
            alert.addButtonWithTitle("Ok")
            alert.delegate = nil
            alert.show()
        }else if self.mobileNumTxtFld.text == ""{
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Enter mobile number"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
            
        }else {
            // Number valid
        

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("LoginOTPViewController") as! LoginOTPViewController
            vc.enterMobNo = self.countryCodeTxtFld.text! + " "+self.mobileNumTxtFld.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
