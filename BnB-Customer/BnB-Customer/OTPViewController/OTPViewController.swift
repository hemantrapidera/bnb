//
//  OTPViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 1/29/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {
    @IBOutlet weak var mobNoTxtFld: UITextField!
    
    @IBOutlet weak var otpTxtFld: UITextField!
    var enteredMobNo = NSString ()

    override func viewDidLoad() {
        super.viewDidLoad()
        mobNoTxtFld.text = enteredMobNo as String
        mobNoTxtFld.delegate = nil
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        self.view.endEditing(true)
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickEditButton(sender: UIButton) {
    }
    @IBAction func onClickResendButton(sender: UIButton) {
    }

    @IBAction func onClickRegisterButton(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("CompleteProfileViewController") as! CompleteProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
