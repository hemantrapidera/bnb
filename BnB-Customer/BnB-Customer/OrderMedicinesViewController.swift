//
//  OrderMedicinesViewController.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/2/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit
import MobileCoreServices

class OrderMedicinesViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ArrowViewDelegate,UITextFieldDelegate,PrescriptionViewDelegate,APICallsDelegate {

    var newMedia: Bool?
    var tempview:UIView!
    var tempview2:UIView!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnPhotos: UIButton!
    @IBOutlet var btnAddText: UIButton!
    @IBOutlet var imgViewPrescription: UIImageView!
    var arrowCount:Int = 0
    var textCount:Int = 0
    var arrowViewsArray:NSMutableArray! = []
    var textViewsArray:NSMutableArray! = []
     @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var topConstraint: NSLayoutConstraint!
    let apiCallsObj = APICalls.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: "imageTapped:")
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        self.imgViewPrescription.userInteractionEnabled = true
        self.imgViewPrescription.addGestureRecognizer(tap)
        
        self.scrollView.scrollEnabled = true
        
       // singltonObj.loginUser("sagar", withPassword: "123")
       // apiCallsObj.delegate = self
       // apiCallsObj.logOutUser()
        
    }
    
    //MARK:- APICallsDelegate Method
    func didSucceedRequest(request:NSURLRequest, withResponse response:NSHTTPURLResponse, andJSONData JSON:AnyObject)
    {
        let dictionary:NSDictionary = JSON as! NSDictionary
        print(dictionary)
    }
    func didFailedRequest(request:NSURLRequest, withResponse response:NSHTTPURLResponse, withError error:NSError, andJSONData JSON:AnyObject){
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imageTapped(sender: UITapGestureRecognizer){
        let touchPoint = sender.locationInView(self.imgViewPrescription)
        
        if self.imgViewPrescription.image == nil{
            return
        }
        for view in arrowViewsArray {
            if (CGRectContainsPoint(view.frame, touchPoint)) {
                return
            }
        }
        for view in textViewsArray {
            if (CGRectContainsPoint(view.frame, touchPoint)) {
                return
            }
        }
        
        arrowCount = arrowCount+1
        let arrow = ArrowView(s: "s", i:(arrowCount),point: touchPoint)
        arrow.delegate = self
        arrow.tag = (arrowCount)
        let longPress = UILongPressGestureRecognizer(target: self, action: "handleLongPress:")
        arrow.addGestureRecognizer(longPress)
        
        self.imgViewPrescription.addSubview(arrow)
        arrowViewsArray.insertObject(arrow, atIndex: 0)
        
        
        // add textfields here and change content size of a scrollview
        let cell = PrescriptionView(frame: CGRect(x: 0, y: (arrowCount-1)*50+2, width: 320, height: 50),i:arrowCount)
        cell.delegate = self
        cell.tag = arrowCount
        cell.txtFldQuantity.tag = arrowCount
        cell.btnDelete.tag = arrowCount
        cell.lblIndex.text = "\(arrowCount)"
        cell.txtFldQuantity.delegate = self
        self.scrollView.addSubview(cell)
        let ht = CGFloat(((arrowCount-1)*50+2) + 50)
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.width, ht)
        for var i = 1; i <= arrowViewsArray.count; i++ {
            if i == arrowCount
            {
                let vw:UIView = self.scrollView.viewWithTag(i)!
                let vie:PrescriptionView = vw as! PrescriptionView
                vie.btnDelete.hidden = false;
            }else{
                let vw:UIView = self.scrollView.viewWithTag(i)!
                let vie:PrescriptionView = vw as! PrescriptionView
                vie.btnDelete.hidden = true;
            }
            
        }
        
        
        
    }
    
    func handleLongPress(longPress: UILongPressGestureRecognizer) {
        
        
        switch longPress.state {
        case .Began:
            let tpoint = longPress.locationInView(self.imgViewPrescription)
            for view in arrowViewsArray {
                let vw:UIView = view as! UIView
                if (CGRectContainsPoint(vw.frame, tpoint)) {
                    tempview = view as! UIView
                }
            }
            break
            
        case .Changed:
            for view in arrowViewsArray {
                if (view as! UIView == tempview){
                    let vw:UIView = view as! UIView
                    vw.center = CGPoint(x: longPress.locationInView(self.imgViewPrescription).x,y: longPress.locationInView(self.imgViewPrescription).y)
                }
            }
            break
        case .Ended:
            tempview  = nil
            break
            
        default:
            break
        }
    }
    
    /*
    func handleLongPressForText(longPress: UILongPressGestureRecognizer) {
        
        
        switch longPress.state {
        case .Began:
            let tpoint = longPress.locationInView(self.imgViewPrescription)
            for view in textViewsArray {
                let vw:UIView = view as! UIView
                if (CGRectContainsPoint(vw.frame, tpoint)) {
                    tempview2 = view as! UIView
                }
            }
            
            
            break
            
        case .Changed:
            for view in textViewsArray {
                if (view as! UIView == tempview2){
                    let vw:UIView = view as! UIView
                    vw.center = CGPoint(x: longPress.locationInView(self.imgViewPrescription).x,y: longPress.locationInView(self.imgViewPrescription).y)
                }
            }
            break
        case .Ended:
            tempview2  = nil
            break
            
        default:
            break
        }
    }
    
*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    /*
    @IBAction func btnAddTextClicked(sender: UIButton) {
    
        textCount = textCount+1
        let addText = AdditionalTextView(i: textCount)
        addText.delegate = self
        let longPress = UILongPressGestureRecognizer(target: self, action: "handleLongPressForText:")
        addText.addGestureRecognizer(longPress)
        addText.tag = textCount
        self.imgViewPrescription.addSubview(addText)
        textViewsArray.insertObject(addText, atIndex: 0)
        
    }
    */
    //MARK: UIButton Action Methods
    @IBAction func btnCameraClicked(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
        
    }
    @IBAction func btnGalleryClicked(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.PhotoLibrary) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
    }
    
    //MARK: UIImagePickerController Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        self.dismissViewControllerAnimated(true, completion: nil)
        if mediaType == (kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            if((self.imgViewPrescription.image) == nil){
                self.imgViewPrescription.image  = image
            }
           
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self,
                    "image:didFinishSavingWithError:contextInfo:", nil)
            } else if mediaType == (kUTTypeMovie as String) {
                // Code to support video here
            }
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .Cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true,
                completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //MARK: ArrowViewDelegate Delegate Methods
    func arrowViewClicked(arrowView: ArrowView, sender:UIButton){
        print("we returned to view controller with tag \(sender.tag)")
        self.removeLastArrowAndTextfieldWithSender(sender)
        
    }
    /*
    func additionalTextViewClicked(txtView: AdditionalTextView, sender:UIButton){
        print("we returned to view controller with tag \(sender.tag)")
        if(sender.tag == textCount){
            
            for view in textViewsArray {
                let vw:UIView = view as! UIView
                if (vw.tag == sender.tag) {
                    print("arrow removed of tag \(sender.tag)")
                    vw.removeFromSuperview()
                    textViewsArray.removeObjectAtIndex(0)
                    textCount = textCount-1
                }
            }
            
        }
        
    }
    */
    //MARK: PrescriptionViewDelegate Delegate Methods
    func prescriptionViewDeleteClicked(txtView: PrescriptionView, sender:UIButton){
        print("prescriptionViewDeleteClicked:: we returned to view controller with tag \(sender.tag)")
        self.removeLastArrowAndTextfieldWithSender(sender)
    }
    
    func removeLastArrowAndTextfieldWithSender(sender:UIButton){
        if(sender.tag == arrowCount){
            
            for view in arrowViewsArray {
                let vw:UIView = view as! UIView
                if (vw.tag == sender.tag) {
                    print("arrow removed of tag \(sender.tag)")
                    vw.removeFromSuperview()
                    arrowViewsArray.removeObjectAtIndex(0)
                    arrowCount = arrowCount-1
                    
                    let vw:UIView = self.scrollView.viewWithTag(sender.tag)!
                    vw.removeFromSuperview()
                    let ht = CGFloat(((arrowCount)*50+2))
                    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.width, ht)
                }
            }
            
            for var i = 1; i <= arrowViewsArray.count; i++ {
                if i == arrowCount
                {
                    let vw:UIView = self.scrollView.viewWithTag(i)!
                    let vie:PrescriptionView = vw as! PrescriptionView
                    vie.btnDelete.hidden = false;
                }else{
                    let vw:UIView = self.scrollView.viewWithTag(i)!
                    let vie:PrescriptionView = vw as! PrescriptionView
                    vie.btnDelete.hidden = true;
                }
                
            }
            
        }
    }
    //MARK: UITextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        self.hideKeyboardCustomMethod(textField)
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.view.frame = CGRectMake(00,-250,self.view.frame.width,self.view.frame.height)
                self.topConstraint.constant = -258
            })
            UIView.commitAnimations()

        return true
    }
    func hideKeyboardCustomMethod(selectedTextfield:UITextField){
        selectedTextfield.resignFirstResponder()
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.frame = CGRectMake(00,00,self.view.frame.width,self.view.frame.height)
            self.topConstraint.constant = 8
        })
    }
    
    
}
