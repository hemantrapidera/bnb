//
//  HomeViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 2/1/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    var selectedCategory = NSMutableArray ()
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func onClickCheckBox(sender: UIButton) {
        
        if sender.tag == 0{
            
            if sender.selected == true{
                selectedCategory .removeObject("GroceryStore")

                sender.selected = false
            }else{
                selectedCategory .addObject("GroceryStore")
                sender.selected = true
            }
        }else if sender.tag == 1 {
            if sender.selected == true{
                selectedCategory .removeObject("MedicalStre")

                sender.selected = false
            }else{
                selectedCategory .addObject("MedicalStre")
                sender.selected = true
            }
        }else if sender.tag == 2 {
            if sender.selected == true{
                selectedCategory .removeObject("Stationary")

                sender.selected = false
            }else{
                 selectedCategory .addObject("Stationary")
                sender.selected = true
            }
        }else{
            
            if sender.selected == true{
                selectedCategory .removeObject("fruits&Vegetables")

                sender.selected = false
            }else{
                selectedCategory .addObject("fruits&Vegetables")
                sender.selected = true
            }
        }
    }
    
    @IBAction func onClickChooseStore(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("SelectStoreViewController") as! SelectStoreViewController
        vc.getCategory = selectedCategory
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickOrderMedicine(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("OrderMedicinesViewController") as! OrderMedicinesViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
