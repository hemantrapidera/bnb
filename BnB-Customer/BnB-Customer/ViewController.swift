//
//  ViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 1/29/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        let alertb = UIAlertView()
        alertb.title = "Message"
        alertb.message = "Please enter OTP"
        alertb.addButtonWithTitle("Ok")
        alertb.delegate = nil
        alertb.show()*/
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning() 
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickLoginButton(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }


    @IBAction func onClickRegisterButton(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

