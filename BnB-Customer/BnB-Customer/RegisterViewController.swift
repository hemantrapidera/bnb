//
//  RegisterViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 2/1/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController,UITextFieldDelegate {

    
    var cCode = NSString ()
    var cCodeLength : Int = 0
    
    @IBOutlet weak var countryCodeTxtFl: UITextField!
    @IBOutlet weak var mobileNumberTextFld: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let cc = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as! String
        let dataSource = CountryCodeDataSource ()
        cCode = dataSource.countryCodeForCountry(cc)
        cCodeLength = cCode.length
        self.countryCodeTxtFl.text = cCode as String
        print(cCode)
    }
    //Phone number validation
    func phoneNumberValidation(value: String) -> Bool {
        let
        charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        var filtered:NSString!
        let inputString:NSArray = value.componentsSeparatedByCharactersInSet(charcter)
        filtered = inputString.componentsJoinedByString("")
        return  value == filtered
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        self.view.endEditing(true)
        return false
    }
    
    
    
    

    @IBAction func OnClickSubmitButton(sender: UIButton) {
        
        if self.phoneNumberValidation(self.mobileNumberTextFld.text!) == false  {
            
            let alert = UIAlertView()
            alert.title = "Message"
            alert.message = "Enter Valid Contact Number"
            alert.addButtonWithTitle("Ok")
            alert.delegate = self
            alert.show()
        }else if self.mobileNumberTextFld.text == ""{
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Enter mobile number"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = self
            alertb.show()
            
        }else {
            // Number valid
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("OTPViewController") as! OTPViewController
            vc.enteredMobNo = self.countryCodeTxtFl.text! + " "+self.mobileNumberTextFld.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

}
