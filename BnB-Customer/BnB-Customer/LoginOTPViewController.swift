//
//  LoginOTPViewController.swift
//  BnB-Customer
//
//  Created by Pooja on 2/1/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

class LoginOTPViewController: UIViewController {
    
    @IBOutlet weak var mNoTextFld: UITextField!
    @IBAction func onClickEditButton(sender: UIButton) {
    }
    @IBOutlet weak var enterOtpTxtFld: UITextField!
    var enterMobNo = NSString ()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mNoTextFld.text = self.enterMobNo as String
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func OnClickLoginButton(sender: UIButton) {
        if self.enterOtpTxtFld.text == "" {
            
            let alertb = UIAlertView()
            alertb.title = "Message"
            alertb.message = "Please enter OTP"
            alertb.addButtonWithTitle("Ok")
            alertb.delegate = nil
            alertb.show()
        
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func onClickLResendOtp(sender: UIButton) {
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        self.view.endEditing(true)
        return false
    }
}
