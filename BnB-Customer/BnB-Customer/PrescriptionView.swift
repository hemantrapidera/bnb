//
//  PrescriptionView.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/2/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit
protocol PrescriptionViewDelegate {
    func prescriptionViewDeleteClicked(view: PrescriptionView, sender:UIButton)
}

class PrescriptionView: UIView {

    @IBOutlet var view:UIView!
    @IBOutlet var lblIndex: UILabel!
    @IBOutlet var txtFldQuantity: UITextField!
    @IBOutlet var btnDelete: UIButton!
    var delegate:PrescriptionViewDelegate!
    var i: Int?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    init(frame: CGRect, i: Int) {
        self.i = i
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let xibFileName = "PrescriptionView" // xib extention not included
        let view = NSBundle.mainBundle().loadNibNamed(xibFileName, owner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }

    
    @IBAction func buttonAction(sender:UIButton){
        print("button clicked with tag: \(sender.tag)")
        delegate?.prescriptionViewDeleteClicked(self, sender: sender)
    }
}
