//
//  ArrowView.swift
//  BnB-Customer
//
//  Created by Sagar Jadhav on 2/2/16.
//  Copyright © 2016 Pooja. All rights reserved.
//

import UIKit

protocol ArrowViewDelegate {
    func arrowViewClicked(arrowView: ArrowView, sender:UIButton)
}

class ArrowView: UIView {

    var delegate:ArrowViewDelegate?
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
       
    }
    

    var s: String?
    var i: Int?
    init(s: String, i: Int, point:CGPoint) {
        self.s = s
        self.i = i
        super.init(frame: CGRect(x: point.x-15, y: point.y-7, width: 30, height: 14))
        self.backgroundColor = UIColor.redColor()
        
        
        let button   = UIButton(type: UIButtonType.Custom) as UIButton
        button.frame = CGRectMake(0,0,30,14)
        button.backgroundColor = UIColor.greenColor()
        button.tag = i
        //button.setTitle("Test Button", forState: UIControlState.Normal)
        button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        self.addSubview(button)
        
        var imageArrow:UIImageView!
        imageArrow  = UIImageView(frame:CGRectMake(0, 0, 14, 14));
        imageArrow.image = UIImage(named:"download.jpeg")
        self.addSubview(imageArrow)
        
        
        let label = UILabel(frame: CGRectMake(14, 0, 16, 14))
        label.textAlignment = NSTextAlignment.Center
        label.font = UIFont.systemFontOfSize(10)
        label.text = "\(i)"
        self.addSubview(label)
        
        
    }
    func buttonAction(sender:UIButton){
        print("button clicked with tag: \(sender.tag)")
        delegate?.arrowViewClicked(self, sender: sender)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.nextResponder()?.touchesBegan(touches, withEvent: event)
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
         self.nextResponder()?.touchesEnded(touches, withEvent: event)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        self.nextResponder()?.touchesMoved(touches, withEvent: event)
       
    }
    
}
